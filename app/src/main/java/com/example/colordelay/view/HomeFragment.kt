package com.example.colordelay.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.colordelay.databinding.FragmentHomeBinding
import com.example.colordelay.viewmodel.ColorViewModel

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.randomButton.setOnClickListener {
            colorViewModel.getColor()
            binding.progressBar.isVisible = true
        }
        colorViewModel.color.observe(viewLifecycleOwner) { color ->
            val path = HomeFragmentDirections.actionHomeFragmentToResultFragment(color)
            binding.progressBar.isVisible = false
            findNavController().navigate(path)
        }
    }


}