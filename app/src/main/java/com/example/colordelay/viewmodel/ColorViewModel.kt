package com.example.colordelay.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colordelay.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {
    private val repo = ColorRepo

    private val _color = MutableLiveData<Int>()
    val color : LiveData<Int> get() = _color

    fun getColor() {
        viewModelScope.launch {
            val color = repo.getColor()
            _color.value = color
        }

    }
}