package com.example.colordelay.model.remote

interface ColorApi {
    suspend fun getColor() : Int
}