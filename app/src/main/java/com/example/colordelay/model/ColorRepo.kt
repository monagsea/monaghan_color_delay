package com.example.colordelay.model

import com.example.colordelay.model.remote.ColorApi
import com.example.colordelay.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

object ColorRepo {
    private val ColorApi = object : ColorApi {
        override suspend fun getColor() = randomColor
    }

    suspend fun getColor(): Int = withContext(Dispatchers.IO)
    {
        delay(5000)
        ColorApi.getColor()
    }
}